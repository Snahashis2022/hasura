import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { GraphQLClient } from 'graphql-request';

@Injectable()
export class HasuraService {
    private client: GraphQLClient;
    constructor(private readonly configService: ConfigService) {
        this.initialize();
    }

    private initialize() {
        const url = this.configService.get<string>('HASURA_URL') as string;
        this.client = new GraphQLClient(url);
    }

    async request<T>(query: string, variables = {}, headers = {}) {
        if (!Object.keys(headers).length) {
            const token = this.configService.get<string>(
                'HASURA_ADMIN_SECRET',
            ) as string;
            this.client.setHeader('x-hasura-admin-secret', `${token}`);
        }
        const data = await this.client.request<T>(query, variables, headers);
        return data;
    }
}
