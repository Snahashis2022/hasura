import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PostModule } from './post/post.module';
import { CommentModule } from './comment/comment.module';
import { UserModule } from './user/user.module';
import { ConfigModule } from '@nestjs/config';
import { ApolloDriver } from '@nestjs/apollo';
import { GraphQLModule } from '@nestjs/graphql';
import { UsersModule as UserssModule } from './graphql/users.module';

import { EventEmitterModule } from '@nestjs/event-emitter'

@Module({
  imports: [
    EventEmitterModule.forRoot(), 
    ConfigModule.forRoot({isGlobal: true}),
    GraphQLModule.forRoot({driver: ApolloDriver,autoSchemaFile: true}), 
    PostModule, 
    CommentModule, 
    UserModule,
    UserssModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
