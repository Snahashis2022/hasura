export interface IDecodedPayload{
    _id: string,
    name: string,
    email: string,
    role: string,
    iat: number,
    exp: number,
    sub: string,
    jti: string
}