export interface IPayload{
    _id: string;
    name: string;
    email: string;
    role: string;
}