import {  Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt'; 
import { SignOptions, sign as Sign, verify as Verify, VerifyOptions, Jwt, JwtPayload, TokenExpiredError } from 'jsonwebtoken';
import { IPayload } from './interface/IPayload';
import { ITokens } from './interface/ITokens';

@Injectable()
export class AuthService {

    public compareHash(data: string, hash: string): boolean {
        return bcrypt.compareSync(data, hash);
    }
    public generateHash(data: string): string {
        const salt = 10;
        const passwordHash = bcrypt.hashSync(data, salt);
        return passwordHash;
    }
    public accessToken(payload: IPayload): string {
        let signOptions: SignOptions = {
            //algorithm: "RS256",
            jwtid: process.env.JWT_ACCESS_TOKEN_ID as string,
            subject: "Validation",
            expiresIn: process.env.ACCESS_TOKEN_EXPIRATION,
        };
        let access_token: string = "Bearer " + Sign(payload, process.env.PRIVATE_KEY as string, signOptions);
        return access_token;
    }
    public refreshToken(payload: IPayload): string {
        let signOptions: SignOptions = {
            //algorithm: "RS256",
            jwtid: process.env.JWT_REFRESH_TOKEN_ID as string,
            subject: "Validation",
            expiresIn: process.env.REFRESH_TOKEN_EXPIRATION,
        };
        let refresh_token: string = "Bearer " + Sign(payload, process.env.PUBLIC_KEY as string, signOptions);
        return refresh_token;
    }
    public getTokens(payload: IPayload): ITokens {
        const accessToken: string = this.accessToken(payload);
        const refreshToken: string = this.refreshToken(payload);

        return {
            access_token: accessToken,
            refresh_token: refreshToken,
            token_type: 'Bearer',
        };
    }
    public async validateAccessToken(bearerHeader: string): Promise< Jwt | JwtPayload> {
        let token: string = bearerHeader.split(" ")[1];
        const verifyOptions: VerifyOptions = {
            //algorithms: ['RS256'],
            jwtid: process.env.JWT_ACCESS_TOKEN_ID as string,
            subject: "Validation",
            maxAge: process.env.ACCESS_TOKEN_EXPIRATION,
            ignoreExpiration: true,
        };
        try {
            const decodedPayload: string | Jwt | JwtPayload = Verify(
                token, process.env.PRIVATE_KEY as string,
                verifyOptions);
            if(typeof decodedPayload === 'string') return undefined;
            else return decodedPayload;
        }
        catch (err) {
            throw new TokenExpiredError("Access token is either expired or invalid", err);
        }
    }
    public async validateRefreshToken(bearerHeader: string): Promise< Jwt | JwtPayload> {
        let token: string = bearerHeader.split(" ")[1];
        const verifyOptions: VerifyOptions = {
            //algorithms: ['RS256'],
            jwtid: process.env.JWT_REFRESH_TOKEN_ID as string,
            subject: "Validation",
            maxAge: process.env.REFRESH_TOKEN_EXPIRATION,
            ignoreExpiration: true,
        };
        try {
            const decodedPayload: string | Jwt | JwtPayload = Verify(
                token, process.env.PUBLIC_KEY as string,
                verifyOptions);
            if(typeof decodedPayload === 'string') return undefined;
            else return decodedPayload;
        }
        catch (err) {
            throw new TokenExpiredError("Refresh token is either expired or invalid", err);
        }
    }

}
