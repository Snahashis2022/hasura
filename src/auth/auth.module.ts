import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthJwtStrategy } from './auth.jwt.strategy';
import { AuthService } from './auth.service';
import jwtOptions from './jwt.config';
@Module({
    imports: [JwtModule.registerAsync(jwtOptions)],
    controllers: [],
    providers: [AuthService, AuthJwtStrategy],
    exports: [AuthService]
})
export class AuthModule { }
