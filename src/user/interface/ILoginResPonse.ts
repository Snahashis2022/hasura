import { ITokens } from "src/auth/interface/ITokens";

export interface ILoginResponse{
    _id: string;
    name: string;
    email: string;
    role : string;
    tokens: ITokens;
}