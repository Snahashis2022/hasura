import { Role } from "../enum/role.enum";

export interface IUser{
    _id: string;
    name: string;
    email: string;
    password : string;
    role: Role;
    refresh_token: string;
    created_at: string;
    updated_at: string;
}