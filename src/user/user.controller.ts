import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { Query } from '@nestjs/graphql';
import { CreateUserDto } from './dto/create-user.dto';
import { LogInUserDto } from './dto/login-user.dto';
import { RefreshTokenDto } from './dto/refresh-token.dto';
import { ILoginResponse } from './interface/ILoginResPonse';
import { UserRepository } from './user.repository';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
    constructor(private readonly _userService: UserService, private readonly _userRepo: UserRepository){}
    @Get("/get/one/:id")
    async getUserById(@Param("id") id: string) {
        return this._userRepo.getUserById(id);
    }
    @Get("/get/all")
    async getAllUser(){
        return this._userRepo.getAllUser();
    }
    @Post("/signUp")
    async signUpUser(@Body() body: CreateUserDto){
        return this._userService.signUpUser(body);
    }
    @Post("/login")
    async logIn(@Body() user: LogInUserDto): Promise<ILoginResponse> {
        let res = await this._userService.logIn(user);
        return res;
    }
    @Post("/refreshToken")
    async sendRefreshToken(@Body() refresh_token: RefreshTokenDto): Promise<ILoginResponse>{
        let res = await this._userService.sendRefreshToken(refresh_token.refresh_token.toString());
        return res;
    }
    @Delete("/logOut/:id")
    async logOut(@Param("id") id: string) {
        let res = await this._userService.logOut(id);
        return res;
    }
}
