import { Injectable } from "@nestjs/common";
import { gql } from "graphql-request";
import { AuthService } from "src/auth/auth.service";
import { HasuraService } from "src/hasura.service";
import { CreateUserDto } from "./dto/create-user.dto";
import { IUser } from "./interface/IUser";


@Injectable()
export class UserRepository {
    private UserFragment = gql`fragment f on user{
        _id
        name
        email
        password
        role
        refresh_token
        created_at
        updated_at
    }`
    constructor(private readonly client: HasuraService, private readonly _auth: AuthService) { }
    private async getUsers(query: string, variables = {}) {
        const res = await this.client.request<any>(
            query,
            variables
        );
        return res;
    }
    async getUserById(id: string): Promise<IUser>{
        const query = gql`
        query ($id: uuid!){
            user: user (where:{_id: {_eq: $id}}) {
            ...f
            }
        }
        ${this.UserFragment}
        `;
        const data = await this.getUsers(query, { id });
        return data;
    }
    async getUserByEmail(email: string): Promise<IUser> {
        const query = gql`
        query ($email: String!){
            user: user (where:{email: {_eq: $email}}) {
                ...f
            }
        }
        ${this.UserFragment}
        `;
        const data = await this.getUsers(query, { email });
        return data;
    }
    async getAllUser(): Promise<IUser> {
        const query = gql`
        {
            users: user {
            ...f
            }
        }
        ${this.UserFragment}`;
        const data = await this.getUsers(query);
        return data;
    }
    async createUser(userObj: CreateUserDto){
        const {name, email, password} = userObj;
        const mutation = gql`
        mutation ($name: String!, $email: String!, $password: String!){
            insert_user_one (object:{name: $name, email: $email, password: $password}) {
            _id
            }
        }
        `
        const data = await this.getUsers(mutation,{name, email, password});
        return data;
    }
    async updateUserRefreshTokenById(userId: string, refresh_token: string){
        
        const mutation = gql`
        mutation ($userId: uuid!, $refresh_token: String!){
            update_user(_set: {refresh_token: $refresh_token}, where: {_id: {_eq: $userId}}) {
                returning {
                    refresh_token
                }
            }
        }
        `
        const data = await this.getUsers(mutation,{userId, refresh_token});
        return data;
    }
    async updateCountById(user_id: string, count: number){
        const mutation = gql`
        mutation ($user_id: uuid!, $count: Int!){
            update_user(_set: {count: $count}, where: {_id: {_eq: $user_id}}) {
                returning {
                    ...f
                }
            }
        }
        ${this.UserFragment}`;
        const data = await this.getUsers(mutation,{user_id, count});
        return data;
    }
    async deleteUserRefreshTokenById(userId: string){
        const mutation = gql`
        mutation ($userId: uuid!){
            deleted_token: update_user(_set: {refresh_token: NULL}, where: {_id: {_eq: $userId}}) {
                returning {
                    refresh_token
                }
            }
        }
        `
        const data = await this.getUsers(mutation,{userId});
        return data;
    }

}