import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { UserRepository } from './user.repository';
import { HasuraService } from 'src/hasura.service';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports:[AuthModule],
  providers: [UserService, UserRepository, HasuraService],
  controllers: [UserController]
})
export class UserModule {}
