import { IsEmail, IsNotEmpty, IsOptional, IsString, MinLength } from "class-validator";

export class UpdateUserDto{
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    name: string;
    
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @IsEmail()
    email: string;

    @IsString()
    @IsOptional()
    @MinLength(5)
    password: string;

    @IsNotEmpty()
    @IsOptional()
    role: string;
}