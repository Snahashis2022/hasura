import { IsString, IsNotEmpty, IsEmail } from "class-validator";

export class LogInUserDto{
    @IsString()
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsString()
    @IsNotEmpty()
    password: string;
}