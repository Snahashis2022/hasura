import { BadRequestException, Injectable, NotAcceptableException, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { AuthService } from 'src/auth/auth.service';
import { IPayload } from 'src/auth/interface/IPayload';
import { ITokens } from 'src/auth/interface/ITokens';
import { CreateUserDto } from './dto/create-user.dto';
import { LogInUserDto } from './dto/login-user.dto';
import { ILoginResponse } from './interface/ILoginResPonse';
import { IUser } from './interface/IUser';
import { UserRepository } from './user.repository';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';

@Injectable()
export class UserService {
    constructor(
        private _eventEmitter: EventEmitter2,
        private readonly _auth: AuthService,
        private readonly _userRepo: UserRepository
        ){}

    async signUpUser(userObj: CreateUserDto){
        let userExists: IUser = await this._userRepo.getUserByEmail(userObj.email);
        if(userExists["user"].length===0){
            userObj.password = this._auth.generateHash(userObj.password);
            const signUpResponse = await this._userRepo.createUser(userObj);
            return signUpResponse;
        }else throw new NotAcceptableException("The email already exists in our database, please try a dfferent one");
    }

    async logIn(user: LogInUserDto): Promise<ILoginResponse>{
        const { email, password } = user;
        const userExistance: IUser = await this._userRepo.getUserByEmail(email);
        if(userExistance["user"].length!==0){
            const userExistanceId: string = userExistance["user"][0]._id;
            const userExistanceName: string = userExistance["user"][0].name;
            const userExistanceEmail: string = userExistance["user"][0].email;
            const userExistancePassword: string = userExistance["user"][0].password;
            const userExistanceRole: string = userExistance["user"][0].role;
            
            const isPasswordMatched: boolean = this._auth.compareHash(password, userExistancePassword);
            if(isPasswordMatched){
                const payload: IPayload = {_id: userExistanceId, name: userExistanceName, email: userExistanceEmail, role: userExistanceRole};
                const tokens: ITokens = this._auth.getTokens(payload);
                const { access_token, refresh_token } = tokens;
                const refreshTokenHash = this._auth.generateHash(refresh_token);
                try{
                    const updated=await this._userRepo.updateUserRefreshTokenById(userExistanceId, refreshTokenHash);
                    return {
                        _id: userExistanceId,
                        name: userExistanceName,
                        email: userExistanceEmail,
                        role: userExistanceRole,
                        tokens: tokens
                    }
                }catch(err){ throw err; }
            }else{
                throw new BadRequestException("Incorrect password!");
            }
        }else{
            throw new NotFoundException("Email does not exist, please sign up");
        }
    }
    async sendRefreshToken(refresh_token: string): Promise<ILoginResponse>{
        const payload:any = await this._auth.validateRefreshToken(refresh_token);
        const user: IUser = await this._userRepo.getUserById( payload._id ); 
        if(this._auth.compareHash(refresh_token, user["user"][0].refresh_token)===false) {
            throw new UnauthorizedException("Invalid token");
        }else{
            const new_access_token = await this._auth.accessToken({_id: payload._id, name: payload.name, email: payload.email, role: payload.role});
            return{
                _id: user._id,
                name: user.name,
                email: user.email,
                role: user.role,
                tokens:{access_token:new_access_token, refresh_token: refresh_token, token_type: "Bearer"}
            }
        }
    }
    async logOut(id: string) {
        const user: IUser = await this._userRepo.getUserById( id ); 
        return this._userRepo.deleteUserRefreshTokenById(id);
    }
    async increaseCount(user_id: string, count: number) {
        const res = await this._userRepo.updateCountById(user_id, count);
        return res;
    }


}
