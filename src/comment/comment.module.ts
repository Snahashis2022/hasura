import { Module } from '@nestjs/common';
import { CommentService } from './comment.service';
import { CommentController } from './comment.controller';
import { CommentRepository } from './comment.repository';
import { HasuraService } from 'src/hasura.service';

@Module({
  providers: [CommentService, CommentRepository, HasuraService],
  controllers: [CommentController]
})
export class CommentModule {}
