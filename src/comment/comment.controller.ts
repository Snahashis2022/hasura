import { Controller, Get, Param } from '@nestjs/common';
import { CommentRepository } from './comment.repository';

@Controller('comment')
export class CommentController {
    constructor(private _commentRepo: CommentRepository){}
    @Get("/get/all/:id")
    async getCommentsByPostId(@Param("id") id: string) {
        return this._commentRepo.getCommentsByPostId(id);
    }
    @Get("/get/all")
    async getAllComments(){
        return this._commentRepo.getAllComments();
    }
}
