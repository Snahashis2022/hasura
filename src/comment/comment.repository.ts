import { Injectable } from "@nestjs/common";
import { gql } from "graphql-request";
import { HasuraService } from "src/hasura.service";


@Injectable()
export class CommentRepository {
    private commentFragment = gql`fragment f on comment{
        
        _id
        comment
        post_id
        user_id
        created_at
        updated_at
        user {
            _id
            email
            name
            password
            created_at
            updated_at
        }

        
    }`
    constructor(private readonly client: HasuraService) { }
    private async getComments(query: string, variables = {}) {
        const res = await this.client.request<any>(
            query,
            variables
        );
        return res;
    }
    async getCommentsByPostId(postId: string) {
        const query = gql`
        query ($postId: uuid!){
            comment: comment (where:{post_id: {_eq: $postId}}) {
            ...f
            }
        }
        ${this.commentFragment}`;
        const data = await this.getComments(query, { postId });
        return data;
    }
    async getAllComments() {
        const query = gql`
        {
            comments: comment {
            ...f
            }
        }
        ${this.commentFragment}`;
        const data = await this.getComments(query);
        return data;
    }


}