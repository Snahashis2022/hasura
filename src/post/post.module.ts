import { Module } from '@nestjs/common';
import { PostService } from './post.service';
import { PostController } from './post.controller';
import { HasuraService } from 'src/hasura.service';
import { PostRepository } from './post.repository';
import { UserService } from 'src/user/user.service';
import { AuthService } from 'src/auth/auth.service';
import { UserRepository } from 'src/user/user.repository';
import { UserModule } from 'src/user/user.module';

@Module({
  providers: [AuthService, PostService, PostRepository, HasuraService, UserService, UserRepository],
  controllers: [PostController]
})
export class PostModule {}
