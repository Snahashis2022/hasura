import { IsString, IsNotEmpty } from "class-validator";

export class PartialCreatePostDto{
    constructor(title: string, description: string){
        this.title = title;
        this.description = description;
    }
    @IsString()
    @IsNotEmpty()
    title: string;

    @IsString()
    @IsNotEmpty()
    description: string;
}