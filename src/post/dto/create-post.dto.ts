import { IsNotEmpty, IsUUID } from "class-validator";
import { PartialCreatePostDto } from "./partial-create-post.dto";


export class CreatePostDto extends PartialCreatePostDto{
    constructor(user_id: string, title: string, description: string){
        super(title, description);
        this.user_id = user_id;
    }
    @IsUUID()
    @IsNotEmpty()
    user_id: string;
}