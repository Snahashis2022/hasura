import { Injectable } from '@nestjs/common';
import { CreatePostDto } from './dto/create-post.dto';
import { PostRepository } from './post.repository';

@Injectable()
export class PostService {
    constructor(private readonly _postRepo: PostRepository){}
    
    async createPost(post: CreatePostDto){
        let createdPost = await this._postRepo.createPost(post); console.log(createdPost);
        if(createdPost !== undefined && createdPost!== null){ 
            let res = await this._postRepo.getPostById(createdPost.created_post._id); 
            if(res !== undefined && res !== null){console.log(res)
                return res;
            }else throw new Error("Something went wrong");
        }else{
            throw new Error("Something went wrong");
        }
    }
}
