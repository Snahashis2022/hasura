import { Injectable } from "@nestjs/common";
import { gql } from "graphql-request";
import { HasuraService } from "src/hasura.service";
import { CreatePostDto } from "./dto/create-post.dto";


@Injectable()
export class PostRepository {
    private postFragment = gql`fragment f on post{
        _id
        title
        description
        user_id
        created_at
        updated_at
        user {
            _id
            email
            name
            password
            count
            created_at
            updated_at
        }
        comments: comments{
            _id
            comment
            user_id
            post_id
            created_at
            updated_at
        }

        
    }`
    constructor(private readonly client: HasuraService) { }
    private async getPosts(query: string, variables = {}) {
        const res = await this.client.request<any>(
            query,
            variables
        );
        return res;
    }
    async getPostById(id: string) {
        const query = gql`
        query ($id: uuid!){
            post: post(where:{_id: {_eq: $id}}) {
            ...f
            }
        }
        ${this.postFragment}`;
        const data = await this.getPosts(query,{id});
        return data;
    }
    async getAllPostsByUserId(userId: string) {
        const query = gql`
        query ($userId: uuid!){
            posts_by_userId: post(where:{user_id: {_eq: $userId}}) {
            ...f
            }
        }
        ${this.postFragment}`;
        const data = await this.getPosts(query,{userId});
        return data;
    }
    async createPost(postDto: CreatePostDto){
        const { user_id, title, description} = postDto;
        const mutation = gql`
        mutation ($user_id:uuid!, $title: String!, $description: String!){
            created_post: insert_post_one (object:{user_id: $user_id, title: $title, description: $description}) {
            _id
            title
            description
            user_id
            }
        }
        ${this.postFragment}`;
        
        try{
            const data = await this.getPosts(mutation,{user_id, title, description});
        return data;
        }catch(err){ throw err; }
    }
    async getAllPosts() {
        const query = gql`
        {
            posts: post {
            ...f
            }
        }
        ${this.postFragment}`;
        const data = await this.getPosts(query);
        return data;
    }
    async countPostByUserId(user_id: string){
        const query = gql`
        query ($user_id: uuid!){
            count: post_aggregate (where:{user_id: {_eq: $user_id}}) {
            aggregate {
                count
            }
            }
        }
        ${this.postFragment}`;
        const data = await this.getPosts(query,{user_id});
        return data.count.aggregate.count;
    }


}