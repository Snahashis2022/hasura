import { Body, Controller, Get, Param, Post, Query, Req, UseGuards } from '@nestjs/common';
import { PostRepository } from './post.repository';
import { PostService } from './post.service';
import { Request } from 'express';
import { GetUser } from 'get-user.decorator';
import { AuthGuard } from '@nestjs/passport';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import { UserService } from 'src/user/user.service';
import { CreatePostDto } from './dto/create-post.dto';
import { PartialCreatePostDto } from './dto/partial-create-post.dto';

@Controller('post')
export class PostController {
    constructor(private readonly _postService: PostService, private _postRepo: PostRepository, private _userService: UserService, private _eventEmitter: EventEmitter2){}

    @UseGuards(AuthGuard("jwt"))
    @Get("/get/one/:id")
    async getPostById(@Param("id") id: string) {
        return this._postRepo.getPostById(id);
    }
    @Get("/get/all")
    @UseGuards(AuthGuard("jwt"))
    async getPostsByUserId(@GetUser() user: any){console.log("j");
        return this._postRepo.getAllPostsByUserId(user._id);
    }
    @Get("/get/all/for-all-users")
    @UseGuards(AuthGuard("jwt"))
    async getAllPosts(){
        return this._postRepo.getAllPosts();
    }
    /* 
    Increases count in user model whenever a new post is inserted by the user
    */
    @Post("/create")
    @UseGuards(AuthGuard("jwt"))
    async createPost(@GetUser() user: any, @Body() body: PartialCreatePostDto) {
        const { title, description } = body;
        let newPost: CreatePostDto = new CreatePostDto(user._id, title, description);
        return await this._postService.createPost(newPost);
    }
    //This is to be exposed so hasura can hit this api when "postInserted" event triggers
    @Post("/event")
    async event(@GetUser() user: any, @Body() body: any) {
        console.log(body.event.data.new.user_id);
        let res = await this._postRepo.countPostByUserId(body.event.data.new.user_id);
        let res2 = await this._userService.increaseCount(body.event.data.new.user_id, res);
        console.log(res2)
        return res2;
    }
}
